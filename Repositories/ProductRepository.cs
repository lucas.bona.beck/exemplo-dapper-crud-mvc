using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using Dapper.Contrib.Extensions;
using exemploDapperCrud.Models;
using Microsoft.Extensions.Configuration;

namespace exemploDapperCrud.Repositories
{
    public class ProductRepository : AbstractRepository<Product>
    {
        public ProductRepository(IDbConnection configuration) : base(configuration) { }

        public new long Add(Product obj)
        {
            return base.Add(obj);
        }
    }
}