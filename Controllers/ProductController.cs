﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using exemploDapperCrud.Models;
using exemploDapperCrud.Repositories;
using System.Net;

namespace exemploDapperCrud.Controllers
{
    public class ProductController : Controller
    {
        private ProductRepository _productRepository;

        public ProductController(ProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public ActionResult Index()
        {
            return View(_productRepository.FindAll());
        }

        public ActionResult Details(int? id)
        {
            if(id == null)
            {
                return StatusCode((int)HttpStatusCode.NotFound);
            }

            Product product = _productRepository.FindById(id.Value);

            if(product == null)
            {
                return StatusCode((int)HttpStatusCode.NotFound);
            }

            return View(product);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Product product)
        {
            if(ModelState.IsValid)
            {
                _productRepository.Add(product);
                return RedirectToAction("Index");
            }

            return View(product);
        }

        public ActionResult Edit(int? id)
        {
            if(id == null)
            {
                return StatusCode((int)HttpStatusCode.BadRequest);
            }

            Product product = _productRepository.FindById(id.Value);

            if(product == null)
            {
                return StatusCode((int)HttpStatusCode.NotFound);
            }

            return View(product);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Product product)
        {
            if(ModelState.IsValid)
            {
                _productRepository.Update(product);
                return RedirectToAction("Index");
            }

            return View(product);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return StatusCode((int)HttpStatusCode.BadRequest);
            }
            Product product = _productRepository.FindById(id.Value);
            if (product == null)
            {
                return StatusCode((int)HttpStatusCode.NotFound);
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            _productRepository.Remove(id);
            return RedirectToAction("Index");
        }
    }
}
